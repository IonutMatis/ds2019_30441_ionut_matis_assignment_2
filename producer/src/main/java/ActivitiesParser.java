import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class ActivitiesParser {
    private final static String ACTIVITIES_FILE = "activities2.txt";
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static int PATIENTS_ID_START_INDEX = 5;
    private final static int PATIENTS_ID_RANGE = 3;
    private final static String QUEUE_NAME = "activities";

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        try {
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            BufferedReader reader = new BufferedReader(new FileReader(ACTIVITIES_FILE));
            String line;
            while ((line = reader.readLine()) != null) {
                String JSONString = createJSONStringFromLine(line);
                channel.basicPublish("", QUEUE_NAME, null, JSONString.getBytes(StandardCharsets.UTF_8));
                System.out.println(JSONString);

                Thread.sleep(1000);
            }
            reader.close();
            channel.close();
            connection.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", ACTIVITIES_FILE);
            e.printStackTrace();
        }
    }

    private static String createJSONStringFromLine(String line) throws Exception {
        String[] lineData = line.split(" ");
        int patientId = createRandomPatientId(PATIENTS_ID_START_INDEX, PATIENTS_ID_RANGE);
        long startTime = createMillisecondsFromDayAndHour(lineData[0], lineData[1]);
        long endTime = createMillisecondsFromDayAndHour(lineData[2], lineData[3]);
        String activity = lineData[4];

        return new JSONObject()
                .put("patient_id", patientId)
                .put("activity", activity)
                .put("start", startTime)
                .put("end", endTime)
                .toString();
    }

    private static long createMillisecondsFromDayAndHour(String day, String hour) throws Exception {
        String dateString = day + " " + hour;
        Date date = DATE_FORMAT.parse(dateString);
        return date.getTime();
    }

    private static int createRandomPatientId(int start, int range) {
        Random random = new Random();
        int randomInt = random.nextInt(range);
        return start + randomInt;
    }
}
