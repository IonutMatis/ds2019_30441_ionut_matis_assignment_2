package com.example.medicalplatform.rabbitmq;

import com.example.medicalplatform.model.Activity;
import com.example.medicalplatform.service.ActivityService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
@RequiredArgsConstructor
public class Consumer {
    private final static Gson gson = new Gson();
    private final ActivityService activityService;

    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public void receiveMessage(byte[] patientActivityAsByteArray) {
        String patientActivity = new String(patientActivityAsByteArray, StandardCharsets.UTF_8);
        Activity activity = gson.fromJson(patientActivity, Activity.class);
        activityService.processActivity(activity);
    }
}