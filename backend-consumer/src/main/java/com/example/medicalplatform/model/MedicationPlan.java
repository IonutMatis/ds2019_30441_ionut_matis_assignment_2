package com.example.medicalplatform.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "medication_plans")
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    private LocalDate creationDate;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "medication_to_medication_plan",
            joinColumns = {@JoinColumn(name = "medication_plan_id")},
            inverseJoinColumns = {@JoinColumn(name = "medication_id")}
    )
    private Set<Medication> medications;

    private String intakeIntervals;

    private LocalDate startDate;

    private LocalDate endDate;
}
