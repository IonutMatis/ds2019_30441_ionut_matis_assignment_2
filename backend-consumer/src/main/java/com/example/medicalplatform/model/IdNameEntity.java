package com.example.medicalplatform.model;

public interface IdNameEntity {
    Long getEntityId();

    void setEntityId(Long id);

    String getEntityName();

    void setEntityName(String name);
}
