package com.example.medicalplatform.seed;

import com.example.medicalplatform.model.*;
import com.example.medicalplatform.repository.MedicationPlanRepository;
import com.example.medicalplatform.repository.MedicationRepository;
import com.example.medicalplatform.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

@Component
@RequiredArgsConstructor
public class ApplicationSeed implements CommandLineRunner {
    private final UserRepository userRepository;
    private final MedicationRepository medicationRepository;
    private final MedicationPlanRepository medicationPlanRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (userRepository.findAll().isEmpty()) {
            userRepository.save(new Doctor("doc1", passwordEncoder.encode("d")));
            userRepository.save(new Doctor("doc2", passwordEncoder.encode("d")));
            Caregiver c1 = new Caregiver("care1", passwordEncoder.encode("c"), LocalDate.of(2000, 3, 10), Gender.FEMALE, "adr", new ArrayList<>());
            Caregiver c2 = new Caregiver("care2", passwordEncoder.encode("c"), LocalDate.of(2000, 3, 10), Gender.FEMALE, "adr", new ArrayList<>());
            Patient p1 = new Patient("pat1", passwordEncoder.encode("p"), LocalDate.of(1997, 6, 24), Gender.MALE, "addr", new ArrayList<>(), c1);
            Patient p2 = new Patient("pat2", passwordEncoder.encode("p"), LocalDate.of(1997, 6, 25), Gender.MALE, "addr", new ArrayList<>(), c2);
            Patient p3 = new Patient("pat3", passwordEncoder.encode("p"), LocalDate.of(1997, 6, 25), Gender.MALE, "addr", new ArrayList<>(), c1);

            c1.getPatients().add(p1);
            c1.getPatients().add(p3);
            c2.getPatients().add(p2);
            p1.setCaregiver(c1);
            p2.setCaregiver(c2);
            p3.setCaregiver(c1);
            userRepository.save(c1);
            userRepository.save(c2);
            userRepository.save(p1);
            userRepository.save(p2);
            userRepository.save(p3);
        }

        if (medicationRepository.findAll().isEmpty()) {
            Medication m1 = new Medication(null, "med1", new LinkedHashSet<>(Arrays.asList("side2", "side3")), "once a day");
            Medication m2 = new Medication(null, "med2", new LinkedHashSet<>(Arrays.asList("side1", "side2")), "twice a day");

            MedicationPlan medicationPlan = new MedicationPlan(null, "med-plan-1", LocalDate.now(), new Patient(5L), new Doctor(1L), new LinkedHashSet<>(), "between 2PM and 3PM", LocalDate.now(), LocalDate.now().plusDays(2));
            medicationPlan.getMedications().add(m1);
            medicationPlan.getMedications().add(m2);

            medicationRepository.save(m1);
            medicationRepository.save(m2);

            medicationPlanRepository.save(medicationPlan);
        }
    }
}
