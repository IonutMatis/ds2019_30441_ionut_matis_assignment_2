package com.example.medicalplatform.config;

import com.example.medicalplatform.model.MyUserDetails;
import com.example.medicalplatform.service.impl.UserLoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserLoginService userLoginService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests().antMatchers("/login", "/api/websocket").permitAll().and()

                .authorizeRequests().antMatchers(HttpMethod.POST, "/patients").hasRole("DOCTOR").and()
                .authorizeRequests().antMatchers(HttpMethod.PUT, "/patients").hasRole("DOCTOR").and()
                .authorizeRequests().antMatchers(HttpMethod.DELETE, "/patients**").hasRole("DOCTOR").and()
                .authorizeRequests().antMatchers(HttpMethod.GET, "/patients**").authenticated().and()

                .authorizeRequests().antMatchers(HttpMethod.POST, "/caregivers").hasAuthority("ROLE_DOCTOR").and()
                .authorizeRequests().antMatchers(HttpMethod.PUT, "/caregivers").hasAuthority("ROLE_DOCTOR").and()
                .authorizeRequests().antMatchers(HttpMethod.DELETE, "/caregivers**").hasAuthority("ROLE_DOCTOR").and()
                .authorizeRequests().antMatchers(HttpMethod.GET, "/caregivers**").authenticated().and()

                .authorizeRequests().antMatchers("/medications**").hasAuthority("ROLE_DOCTOR").and()
                .authorizeRequests().antMatchers("/medication-plans**").hasRole("DOCTOR").and()

                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler())
                .authenticationEntryPoint(authenticationEntryPoint()).and()

                .httpBasic().and()

                .formLogin()
                .loginProcessingUrl("/login")
                .successHandler(successHandler())
                .failureHandler(failureHandler()).and()


                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .cors().and()
                .csrf().disable();
    }

    private AuthenticationSuccessHandler successHandler() {
        return (httpServletRequest, httpServletResponse, authentication) -> {
            Long id = ((MyUserDetails) authentication.getPrincipal()).getUser().getId();
            String role = ((MyUserDetails) authentication.getPrincipal()).getUser().getRole().toString();
            String userJson = "{\n" +
                    "  \"id\": \"" + id + "\",\n" +
                    "  \"role\": \"" + role + "\"\n" +
                    "}";
            httpServletResponse.setStatus(200);
            httpServletResponse.getWriter().append(userJson);
        };
    }

    private AuthenticationFailureHandler failureHandler() {
        return (httpServletRequest, httpServletResponse, e) -> {
            httpServletResponse.getWriter().append("Authentication failure. Invalid username or password!");
            httpServletResponse.setStatus(401);
        };
    }

    private AccessDeniedHandler accessDeniedHandler() {
        return (httpServletRequest, httpServletResponse, e) -> {
            httpServletResponse.getWriter().append("Access denied!");
            httpServletResponse.setStatus(403);
        };
    }

    private AuthenticationEntryPoint authenticationEntryPoint() {
        return (httpServletRequest, httpServletResponse, e) -> {
            httpServletResponse.getWriter().append("Not authenticated!");
            httpServletResponse.setStatus(401);
        };
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userLoginService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
