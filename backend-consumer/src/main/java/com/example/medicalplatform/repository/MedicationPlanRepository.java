package com.example.medicalplatform.repository;

import com.example.medicalplatform.model.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Long> {
}