package com.example.medicalplatform.service.impl;

import com.example.medicalplatform.exception.UnknownDoctorException;
import com.example.medicalplatform.model.Doctor;
import com.example.medicalplatform.model.MyUserDetails;
import com.example.medicalplatform.model.User;
import com.example.medicalplatform.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.omg.CORBA.UnknownUserException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserLoginService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User foundUser = userRepository.findByName(name).
                orElseThrow(() -> new UsernameNotFoundException("Unknown user"));

        return new MyUserDetails(foundUser);
    }
}
