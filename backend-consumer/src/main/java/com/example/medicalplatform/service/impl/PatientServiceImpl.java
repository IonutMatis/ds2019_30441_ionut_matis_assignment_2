package com.example.medicalplatform.service.impl;

import com.example.medicalplatform.dto.medicationPlan.MedicationPlanDTO;
import com.example.medicalplatform.dto.patient.PatientCreateUpdateDTO;
import com.example.medicalplatform.dto.patient.PatientDetailedViewDTO;
import com.example.medicalplatform.dto.patient.PatientSimpleViewDTO;
import com.example.medicalplatform.exception.UnknownCaregiverException;
import com.example.medicalplatform.exception.UnknownPatientException;
import com.example.medicalplatform.model.Caregiver;
import com.example.medicalplatform.model.Patient;
import com.example.medicalplatform.repository.CaregiverRepository;
import com.example.medicalplatform.repository.MedicationPlanRepository;
import com.example.medicalplatform.repository.PatientRepository;
import com.example.medicalplatform.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class PatientServiceImpl implements PatientService {
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;
    private final MedicationPlanRepository medicationPlanRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public PatientSimpleViewDTO saveUpdate(PatientCreateUpdateDTO patientCreateUpdateDTO, Long id) {
        Patient patient = PatientCreateUpdateDTO.generateModelFromDTO(patientCreateUpdateDTO);
        if (!"".equals(patientCreateUpdateDTO.getPassword())) {
            String unencodedPass = patientCreateUpdateDTO.getPassword();
            patient.setPassword(passwordEncoder.encode(unencodedPass));
        } else {
            if (id != null) {
                Patient foundPatient = patientRepository.findById(id).get();
                patient.setPassword(foundPatient.getPassword());
            }
        }
        if (id != null) {
            patient.setId(id);
        }
        if (patientCreateUpdateDTO.getCaregiver() != null) {
            Caregiver foundCaregiver = caregiverRepository.findById(patientCreateUpdateDTO.getCaregiver().getId())
                    .orElseThrow(UnknownCaregiverException::new);
            patient.setCaregiver(foundCaregiver);
        } else {
            patient.setCaregiver(null);
        }
        Patient savedPatient = patientRepository.save(patient);

        return PatientSimpleViewDTO.generateDTOFromModel(savedPatient);
    }

    @Override
    public PatientDetailedViewDTO findById(Long id) {
        Patient foundPatient = patientRepository.findById(id).orElseThrow(UnknownPatientException::new);

        return PatientDetailedViewDTO.generateDTOFromModel(foundPatient);
    }

    @Override
    public List<PatientSimpleViewDTO> findAll() {
        return patientRepository.findAll().stream()
                .map(PatientSimpleViewDTO::generateDTOFromModel)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        medicationPlanRepository.findAll().stream()
                .filter(medicationPlan -> medicationPlan.getPatient().getId().equals(id))
                .forEach(medicationPlan -> {
                    medicationPlan.getPatient().setMedicalRecord(new ArrayList<>());
                    medicationPlan.setPatient(null);
                    medicationPlan.setDoctor(null);
                    medicationPlanRepository.delete(medicationPlan);
                });
        patientRepository.deleteById(id);
    }

    @Override
    public List<MedicationPlanDTO> getMedicationPlansForPatient(Long id) {
        return medicationPlanRepository.findAll().stream()
                .filter(medicationPlan -> medicationPlan.getPatient().getId().equals(id))
                .map(MedicationPlanDTO::generateDTOFromModel)
                .collect(Collectors.toList());
    }
}
