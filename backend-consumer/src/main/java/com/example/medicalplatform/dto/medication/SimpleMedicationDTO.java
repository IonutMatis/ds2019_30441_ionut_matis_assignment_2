package com.example.medicalplatform.dto.medication;

import com.example.medicalplatform.model.Medication;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SimpleMedicationDTO {
    private Long id;
    private String name;

    public static SimpleMedicationDTO generateDTOFromModel(Medication medication) {
        SimpleMedicationDTO dto = new SimpleMedicationDTO();
        dto.setId(medication.getId());
        dto.setName(medication.getName());

        return dto;
    }
}
