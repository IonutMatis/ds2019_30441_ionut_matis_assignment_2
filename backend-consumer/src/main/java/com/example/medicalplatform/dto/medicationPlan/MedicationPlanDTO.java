package com.example.medicalplatform.dto.medicationPlan;

import com.example.medicalplatform.dto.SimpleUserDTO;
import com.example.medicalplatform.dto.medication.SimpleMedicationDTO;
import com.example.medicalplatform.model.Doctor;
import com.example.medicalplatform.model.MedicationPlan;
import com.example.medicalplatform.model.Patient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MedicationPlanDTO {
    private Long id;
    private String description;
    private LocalDate creationDate;
    private SimpleUserDTO<Patient> patient;
    private SimpleUserDTO<Doctor> doctor;
    private Set<SimpleMedicationDTO> medications;
    private String intakeIntervals;
    private LocalDate startDate;
    private LocalDate endDate;

    public static MedicationPlan generateModelFromDTO(MedicationPlanDTO dto) {
        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan.setId(dto.getId());
        medicationPlan.setDescription(dto.getDescription());
        medicationPlan.setCreationDate(LocalDate.now());
        medicationPlan.setMedications(new LinkedHashSet<>());
        medicationPlan.setIntakeIntervals(dto.getIntakeIntervals());
        medicationPlan.setStartDate(dto.getStartDate());
        medicationPlan.setEndDate(dto.getEndDate());

        return medicationPlan;
    }

    public static MedicationPlanDTO generateDTOFromModel(MedicationPlan medicationPlan) {
        MedicationPlanDTO dto = new MedicationPlanDTO();
        dto.setId(medicationPlan.getId());
        dto.setDescription(medicationPlan.getDescription());
        dto.setCreationDate(medicationPlan.getCreationDate());
        dto.setPatient(SimpleUserDTO.generateDTOFromModel(medicationPlan.getPatient()));
        dto.setDoctor(SimpleUserDTO.generateDTOFromModel(medicationPlan.getDoctor()));
        dto.setMedications(medicationPlan.getMedications().stream()
                .map(SimpleMedicationDTO::generateDTOFromModel)
                .collect(Collectors.toSet()));
        dto.setIntakeIntervals(medicationPlan.getIntakeIntervals());
        dto.setStartDate(medicationPlan.getStartDate());
        dto.setEndDate(medicationPlan.getEndDate());

        return dto;
    }
}