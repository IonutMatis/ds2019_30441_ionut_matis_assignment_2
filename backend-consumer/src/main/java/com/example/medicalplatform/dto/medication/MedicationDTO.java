package com.example.medicalplatform.dto.medication;

import com.example.medicalplatform.model.Medication;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MedicationDTO {
    private Long id;
    private String name;
    private List<String> sideEffects;
    private String dosage;

    public static Medication generateModelFromDTO(MedicationDTO dto) {
        Medication medication = new Medication();
        medication.setId(dto.getId());
        medication.setName(dto.getName());
        medication.setDosage(dto.getDosage());

        return medication;
    }

    public static MedicationDTO generateDTOFromModel(Medication medication) {
        MedicationDTO dto = new MedicationDTO();
        dto.setId(medication.getId());
        dto.setName(medication.getName());
        dto.setSideEffects(new ArrayList<>(medication.getSideEffects()));
        dto.setDosage(medication.getDosage());

        return dto;
    }
}
