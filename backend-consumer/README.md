    **Project Setup (after clone)**

1.	Clone the project
2.	Enter backend folder
3.	Import the project in your IDE
4.	Create a database “ds_assign1” in MySQL
5.	Change line 7 in application.properties (/src/main/resources) to your DB password
6.	Change line 22 in application.properties (/src/main/resources) to allow table creation in the DB: spring.jpa.hibernate.dll-auto=create
7.	Run the application
8.	Check if the tables were created
9.	Change line 22 in application.properties (/src/main/resources) to spring.jpa.hibernate.dll-auto=validate
