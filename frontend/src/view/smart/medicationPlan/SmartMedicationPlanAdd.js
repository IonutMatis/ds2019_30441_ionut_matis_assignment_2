import React, {Component} from 'react';
import medicationPlanModel from "../../../model/medicationPlanModel";
import MedicationPlanAddEdit from "../../dumb/medicationPlan/MedicationPlanAddEdit";
import medicationPlanPresenter from "../../../presenter/medicationPlanPresenter";
import userModel from "../../../model/userModel";
import patientPresenter from "../../../presenter/patientPresenter";
import patientModel from "../../../model/patientModel";
import medicationModel from "../../../model/medicationModel";
import medicationPresenter from "../../../presenter/medicationPresenter";

const mapModelStateToComponentState = (medicationPlanModelState) => ({
    newMedicationPlan: medicationPlanModelState.newMedicationPlan
});

export default class SmartMedicationPlanAdd extends Component {
    constructor(props) {
        super(props);
        patientPresenter.onInit();
        medicationPresenter.onInit();

        this.state = mapModelStateToComponentState(medicationPlanModel.state);
        this.listener = () =>
            this.setState(mapModelStateToComponentState(medicationPlanModel.state));
        medicationPlanModel.addListener("changeMedicationPlan", this.listener);
        patientModel.addListener("changePatient", this.listener);
        medicationModel.addListener("changeMedication", this.listener);
    }

    componentWillUnmount() {
        medicationPlanModel.removeListener("changeMedicationPlan", this.listener);
        patientModel.removeListener("changePatient", this.listener);
        medicationModel.removeListener("changeMedication", this.listener);
    }

    render() {
        return (
            <MedicationPlanAddEdit onChange={medicationPlanPresenter.onChange}
                                   onSubmit={medicationPlanPresenter.onCreate}

                                   medicationPlan={this.state.newMedicationPlan}
                                   patients={patientModel.state.patients}
                                   allMedications={medicationModel.state.medications}

                                   mainText="Add a medication plan"
                                   userModelState={userModel.state}/>
        )
    }
}