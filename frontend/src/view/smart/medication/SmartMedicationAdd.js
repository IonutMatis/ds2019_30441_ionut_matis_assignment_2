import React, {Component} from 'react';
import medicationModel from "../../../model/medicationModel";
import MedicationAddEdit from "../../dumb/medication/MedicationAddEdit";
import medicationPresenter from "../../../presenter/medicationPresenter";
import userModel from "../../../model/userModel";

const mapModelStateToComponentState = (medicationModelState) => ({
    newMedication: medicationModelState.newMedication
});

export default class SmartMedicationAdd extends Component {
    constructor(props) {
        super(props);

        this.state = mapModelStateToComponentState(medicationModel.state);
        this.listener = (medicationModelState) =>
            this.setState(mapModelStateToComponentState(medicationModelState));
        medicationModel.addListener("changeMedication", this.listener);
    }

    componentWillUnmount() {
        medicationModel.removeListener("changeMedication", this.listener);
    }

    render() {
        return (
            <MedicationAddEdit onChange={medicationPresenter.onChange}
                               onSubmit={medicationPresenter.onCreate}
                               medication={this.state.newMedication}

                               mainText="Add a medication"
                               userModelState={userModel.state}/>
        )
    }
}