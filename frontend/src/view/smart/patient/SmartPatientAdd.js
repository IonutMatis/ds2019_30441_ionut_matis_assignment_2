import React, {Component} from 'react';
import patientModel from "../../../model/patientModel";
import PatientAddEdit from "../../dumb/patient/PatientAddEdit";
import patientPresenter from "../../../presenter/patientPresenter";
import userModel from "../../../model/userModel";
import caregiverPresenter from "../../../presenter/caregiverPresenter";
import caregiverModel from "../../../model/caregiverModel";

const mapModelStateToComponentState = (patientModelState, caregiverModelState) => ({
    caregivers: caregiverModelState.caregivers,
    newPatient: patientModelState.newPatient
});

export default class SmartPatientAdd extends Component {
    constructor(props) {
        super(props);
        caregiverPresenter.onInit();
        patientPresenter.onInit();

        this.state = mapModelStateToComponentState(patientModel.state, caregiverModel.state);
        this.listener = () =>
            this.setState(mapModelStateToComponentState(patientModel.state, caregiverModel.state));
        patientModel.addListener("changePatient", this.listener);
        caregiverModel.addListener("changeCaregiver", this.listener)
    }

    componentWillUnmount() {
        patientModel.removeListener("changePatient", this.listener);
        caregiverModel.removeListener("changeCaregiver", this.listener)
    }

    render() {
        return (
            <PatientAddEdit onChange={patientPresenter.onChange}
                            onSubmit={patientPresenter.onCreate}
                            patient={this.state.newPatient}
                            caregivers={caregiverModel.state.caregivers}

                            mainText="Add a patient"
                            userModelState={userModel.state}/>
        );
    }
}