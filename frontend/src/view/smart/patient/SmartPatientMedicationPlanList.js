import React, {Component} from "react";
import patientModel from "../../../model/patientModel";
import userModel from "../../../model/userModel";
import patientPresenter from "../../../presenter/patientPresenter";
import PatientMedicationPlanList from "../../dumb/patient/PatientMedicationPlanList";

const mapModelStateToComponentState = (patientModelState) => ({
    medicationPlans: patientModelState.medicationPlans
});

export default class SmartPatientMedicationPlanList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            medicationPlans: []
        };
        patientPresenter.onLoadMedicationPlans()
            .then(() => {
                this.state = mapModelStateToComponentState(patientModel.state);
            });

        this.listener = () => this.setState(mapModelStateToComponentState(patientModel.state));
        patientModel.addListener("changePatient", this.listener);
    }

    componentWillUnmount() {
        patientModel.removeListener("changePatient", this.listener);
    }

    render() {
        return (
            <PatientMedicationPlanList userModelState={userModel.state}
                                       medicationPlans={this.state.medicationPlans}/>
        );
    }
}