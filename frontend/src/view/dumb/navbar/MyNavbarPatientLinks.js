import React from 'react';

const MyNavbarPatientLinks = ({userModelState}) => (
    <ul className="navbar-nav mr-auto">
        <li className={"nav-item " + ((window.location.hash.includes(userModelState.currentUser.role)) ? "active" : "")}>
            <span className="nav-link" onClick={() => window.location.assign("#/" + userModelState.currentUser.role)}>
                Home
            </span>
        </li>
        <li className={"nav-item " + ((window.location.hash.includes('#/my-medication-plans')) ? "active" : "")}>
            <span className="nav-link" onClick={() => window.location.assign("#/my-medication-plans")}>
                My medication plans
            </span>
        </li>
        <li className={"nav-item " + ((window.location.hash.includes('#/my-account')) ? "active" : "")}>
            <span className="nav-link" onClick={() => window.location.assign("#/my-account")}>
                My Account
            </span>
        </li>
    </ul>
);

export default MyNavbarPatientLinks;