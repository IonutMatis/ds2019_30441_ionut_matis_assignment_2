import React from 'react';

const MyNavbarCaregiverLinks = ({userModelState}) => (
    <ul className="navbar-nav mr-auto">
        <li className={"nav-item " + ((window.location.hash.includes(userModelState.currentUser.role)) ? "active" : "")}>
            <span className="nav-link" onClick={() => window.location.assign("#/" + userModelState.currentUser.role)}>
                Home
            </span>
        </li>
        <li className={"nav-item " + ((window.location.hash.includes('#/my-patients')) ? "active" : "")}>
            <span className="nav-link" onClick={() => window.location.assign("#/my-patients")}>
                My patients
            </span>
        </li>
    </ul>
);

export default MyNavbarCaregiverLinks;