import React from 'react';
import "../../style/Login.css";

const Login = ({invalidUsernameOrPassword, onFormSubmit, currentUser, onChange}) => (
    <div className="login-form">
        {
            invalidUsernameOrPassword === true ?
                <div className="alert alert-danger" role="alert">
                    Invalid username or password!
                </div>
                :
                <div/>
        }
        <h2 className="text-center">Log in</h2>
        <div className="form-group">
            <input type="text" className="form-control" placeholder="Username" required="required"
                   value={currentUser.username}
                   onChange={e => onChange("username", e.target.value)}/>
        </div>
        <div className="form-group">
            <input type="password" className="form-control" placeholder="Password" required="required"
                   value={currentUser.password}
                   onChange={e => onChange("password", e.target.value)}
                   onKeyDown={e => e.key === 'Enter' ? onFormSubmit() : ''}/>
        </div>
        <div className="form-group">
            <button onClick={onFormSubmit} className="btn btn-primary btn-block my-btn">Log in</button>
        </div>
    </div>
);

export default Login;